
import styled from "styled-components";

export const StyledButton = styled.button`
    padding: 1px 16px;
    text-transform: uppercase;
    font-weight: bolder;
    border-radius: 5px;
    background-color: black;
    border-color: black;
    box-shadow: 0 1px 3px rgb(0 0 0 / 12%), 0 1px 2px rgb(0 0 0 / 24%);
    outline: none;
    margin-top: 20px;
`