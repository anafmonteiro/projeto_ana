import styled from "styled-components";

import Space from '../../../Images/space.jpg'

export const StyledBackground = styled.body`
    background-image: url(${Space});
    background-position: center;
    background-size: cover;
    background-repeat: repeat-y;
    height: 1000px;
`