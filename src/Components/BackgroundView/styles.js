import styled from "styled-components";

import Space from '../../Images/space.jpg'

export const StyledBackgroundView = styled.body`
    background-image: url(${Space});
    height: 100%;
`