import axios from "axios";

const Calculate = async (value) => {
    try{

        let resp = await axios.get(`https://swapi.dev/api/starships/?value=${value}`)
        let result = resp.data.results
        let page = 2
        while(resp.data.next != null){ 
            resp = await axios.get(`https://swapi.dev/api/starships/?value=${value}&page=${page}`)
            result = result.concat(resp.data.results)
            page ++
        }
        
        result.forEach(element => {
            if(element.MGLT != "unknown"){
                let timeToStop = element.consumables && element.consumables.split(' ')[0]
                if(element.consumables && element.consumables.split(' ')[1]){
                    let time 
                    switch(element.consumables.split(' ')[1]){
                        case 'years' :
                        case 'year':
                            time = 8760
                            break
                        case 'months':
                        case 'month':
                            time = 730
                            break
                        case 'weeks':
                        case 'week':
                            time = 168
                            break
                        case 'days':
                        case 'day':
                            time = 24
                            break
                        default:
                            time = time
                            break
                    }

                    timeToStop = parseInt(timeToStop) * time
                    element.stops = Math.trunc((value / parseFloat(element.MGLT)) / timeToStop)
                }
            }
        });

        return result   
        
    }catch(e){
        return {"message": "error"}
    }
    
}


export default Calculate