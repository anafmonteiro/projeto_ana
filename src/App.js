import React from "react";

import MainPage from "../src/Pages/MainPage";

function App() {
  return (
    <MainPage />
  );
}

export default App;