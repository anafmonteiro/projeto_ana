import React, { useState, useEffect } from 'react';

import {Header} from '../Components/Button/Header/index.js';

import Image from '../Images/DarthVader.png';

import ImageYoda from '../Images/yoda.png';

import {Background} from '../Components/Button/Background/index.js';

import {View} from '../Components/Button/View/index.js';

import {Input} from '../Components/Button/Input/index.js'; 

import {Button} from '../Components/Button/index.js';

import {Title} from '../Components/Button/Title/index.js';

import {Text} from '../Components/Button/Text/index.js';

import {CardView} from '../Components/Button/CardView/index.js';

import {BackgroundView} from '../Components/BackgroundView/index.js';

import { Roller } from "react-awesome-spinners";

import Calculate from '../Services/calculate';


const MainPage = () => {

    const [inputValue, setInputValue] = useState()
    const [loading, setloading] = useState(false)
    const [result, setResult] = useState([])
    const [message, setMessage] = useState('')
    const [value, setValue] = useState(false)

    const handleInput = (value) => {

        setInputValue(value)
 
    }
    
    const getNaves = async () => {
        setloading(true)
        if(inputValue){
            if(isNaN(inputValue)){
                setMessage('Por favor, insira um valor numérico (em MGLT)!')
                setValue(true)
                setloading(true)
                setTimeout(() => {
                    setMessage('')
                    setValue(false)
                    setloading(false)
                }, 2000)
            }else{
                const naves = await Calculate(inputValue)
                setResult(naves)
            }
        }else{
            setMessage('Por favor, insira um valor válido!')
            setValue(true)
            setloading(true)
            setTimeout(() => {
                setMessage('')
                setValue(false)
                setloading(false)
            }, 2000)
        }
        setloading(false)
    }

    const showMessage = () => {
        setMessage('Clique para inserir um outro valor!')
            setValue(true)
            setloading(true)
            setTimeout(() => {
                setMessage('')
                setValue(false)
                setloading(false)
            }, 2000)
    }
    return(
        <div >
            {result.length<=0?
                <Background>
                    <Header>
                        <img src={Image} style={{width:100}}></img>
                    </Header>
                        <View>
                            {loading? 
                                <Roller color='white'/>
                                :
                                value?
                                <>
                                    <Text>{message}</Text>
                                    <View>
                                        <Roller color='white' />
                                    </View>
                                </>
                                :
                                <>
                                    <Title>
                                        Escreva abaixo um valor (em MGLT):
                                    </Title>
                                    <Input 
                                        onChange={(e)=>handleInput(e.target.value)}
                                    />
                                    <Button onClick={()=>getNaves()}>
                                        <Text>
                                            Calcular
                                        </Text>
                                    </Button>
                                </>
                            }
                        </View>
                </Background>
            :
                <BackgroundView>
                    <Header onClick={()=>window.location.reload()} >
                        {value?
                            <>
                                <View>
                                    <img src={ImageYoda} style={{width:150}}></img>
                                </View>
                                <Text>{message}</Text>
                            </>
                            :
                            <img src={ImageYoda} style={{width:150}} onMouseOver={()=>showMessage()}></img>
                        }
                    </Header>
                    <View>
                       {
                           result && result.map(element=>{
                               return(
                                   <View>
                                    <CardView>
                                        <View>
                                            <Title>
                                                <b>{element.name || ''}</b>
                                            </Title>
                                            <Title>
                                                MGLT por hora: {element.MGLT ? element.MGLT == 'unknown'? "Ainda é um mistério..." : element.MGLT : ''}
                                            </Title>
                                            <Title>
                                                Paradas necessárias : { element.MGLT == 'unknown'? "Não é possível calcular" : element.stops != (null && undefined)?  element.stops : '' }
                                            </Title>
                                        </View>
                                    </CardView>
                                    </View>
                               )
                           })
                       }
                    </View>
                </BackgroundView>
                }
        </div>
    )

}
export default MainPage